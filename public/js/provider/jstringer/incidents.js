import { fetchAll } from '../ajax/incidents/fetchAll.js'
import moment from '../../vendor/moment.js'

const fetchIncidentsByType = (incidentType) => {
   return fetchAll().filter(incident => (incident.asset === 'alpha' && incident.type === incidentType && moment(incident.date).isAfter(moment().subtract(1, "year"))))
}

const groupIncidentsByMonth = (incidents) => {
    let bucket = [];
    incidents.forEach(incident => {
        let month = parseInt(moment(incident.date).format('MM'))
        let bucketItem = bucket.find(item => (item, month == parseInt(moment(item.date).format('MM'))))
        if (bucketItem) {
            bucketItem.incidents.push(incident);
        } else {
            bucket.push({
                date: incident.date,
                incidents: [incident],
            })
        }
    })
    return bucket;
}

export const getSpeedingIncidents = () => {
    let speedingIncidents = fetchIncidentsByType('overspeed');
    return groupIncidentsByMonth(speedingIncidents);
}

export const getIdleIncidents = () => {
    let idleIncidents = fetchIncidentsByType('idle');
    return groupIncidentsByMonth(idleIncidents);
}

export const getStartingLocation = () => {
    const ignitionOffIncidents = fetchIncidentsByType('ignitionOff');
    const oneYearAgoToday = moment().subtract(11, 'months').startOf('month');
    let lastYearIncidents = ignitionOffIncidents.filter(incident => {
        return (moment(incident.date).isSameOrAfter(oneYearAgoToday))
    })

    const firstIncident = lastYearIncidents.sort((incidentA, incidentB) => {
        if(incidentA.date < incidentB.date){
            return -1
        }else if (incidentA.date > incidentB.date){
            return 1
        }else{
            return 0
        }
    })[0]

    return firstIncident.location
}

export const getEndingLocation = () => {
    const ignitionOffIncidents = fetchIncidentsByType('ignitionOff');
    const today = moment().startOf('day');
    let todaysIncidents = ignitionOffIncidents.filter(incident => {
        return (moment(incident.date).isSameOrAfter(today))
    })

    const lastIncident = todaysIncidents.sort((incidentA, incidentB) => {
        if(incidentA.date < incidentB.date){
            return -1
        }else if (incidentA.date > incidentB.date){
            return 1
        }else{
            return 0
        }
    })[todaysIncidents.length-1]

    return lastIncident.location
}