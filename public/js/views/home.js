import Templates from "../Templates.js";
import { getSpeedingIncidents, getIdleIncidents, getStartingLocation, getEndingLocation } from '../provider/jstringer/incidents.js';

// import yourWayForIdling from "../../../provider/you/?";
// import yourWayForStart from "../../../provider/you/?";
// import yourWayForEnd from "../../../provider/you/?";

import SpeedingBarChartComponent from "../components/SpeedingBarChart.js";
import IdlingBarChartComponent from "../components/IdlingBarChart.js";
import MapComponent from "../components/LeafletMap.js";

var HomeView = {
	"template": Templates.getView( "home" ),
	data(){
		return {
			"speedingMonths": [],
			"idlingMonths": [],
			"startCenter": {
				"latitude": 0,
				"longitude": 0
			},
			"endCenter": {
				"latitude": 0,
				"longitude": 0
			}
		};
	},
	"components": {
		"map": MapComponent,
		"speeding-bar-chart": SpeedingBarChartComponent,
		"idling-bar-chart": IdlingBarChartComponent
	},

	"on": {
		init(){
			console.log(getIdleIncidents());
			this.set( "speedingMonths", getSpeedingIncidents() );
			this.set( "idlingMonths", getIdleIncidents() );
			this.set( "startCenter", getStartingLocation() );
			this.set( "endCenter", getEndingLocation() );
		},
		complete(){
			setTimeout( () => {
				this
					.findAllComponents( "map" )
					.forEach( ( map ) => map.redraw() );
			}, 0 );
		}
	}
};

export default HomeView;
