# Front End Interview Project

## Tips

* View this file in a full fledged (GFM) markdown viewer like [Dillinger](http://dillinger.io/). You will not regret it.

## Basics

In the root of the project, run `npm install` to get the dependencies installed.  
1. To create a useful application environment: `npm run setup`
2. To start a simple development server (which will be available at [localhost:9876](localhost:9876)): `npm start`

There are two files at the root: `suggested.editorconfig` and `suggested.eslintrc`.  
If you remove `suggested` from both, you could opt into linting and [automatic editor configuration](https://editorconfig.org/#download), but this is **strictly not required**, it is simply an option if you'd like to use it.

## Definitions

* ISO Date
    * A datetime string conforming to the [full ISO8601 combined format][iso8601]
    * Example: `2017-04-06T22:23:26+00:00`
* Asset
    * A vehicle, piece of equipment, or other tracked resource
    * Will be a string
    * Will always be one of: `alpha`, `beta`, `gamma`, `delta`
* Incident Type
    * A simple representation - with no extra information - of an event that occurred
    * Will be a string
    * Will always be one of: `idle`, `overspeed`, `ignitionOff`, `impact`
* Location
    * A set of geo-coordinates to locate an asset
    * Will be represented as an object with the keys `latitude` and `longitude`
    * The values will be valid float number coordinates on the Earth
    * Example:
        ```
        {
            "latitude": 123.4567
            "longitude": 56.7890
        }
        ```
* Incident
    * An event that occurred, including extra information
    * Will be an object with the keys `asset`, `type`, `date`, `location`
    * The object values will have the following types (see list of definitions above):
        ```
        {
            "asset": Asset,
            "type": Incident Type,
            "date": ISO Date,
            "location": Location
        }
        ```
* "1 year ago / the last year"
    * The first second of the first day of the month found by ( today - 11 months ) up to *any* time today.    
    * Example: If today is April 6th, 2017 and it is 5PM:
        * "1 year ago" is May 1st, 2016 at exactly 00:00:00.
        * "the last year" is everything from 00:00:00 on May 1st, 2016 to April 6th, 2017 at 11:59:59 PM

## Your Goals

1. Provide data to populate the speeding chart for Asset "alpha" for the last year
2. Provide data to populate the idling chart for Asset "alpha" for the last year
3. Provide data to show the starting location 1 year ago for Asset "alpha" on the map
4. Provide data to show the ending location today for Asset "alpha" on the map
5. Use only asynchronous code or Observable data streams

### Business Reasoning

For clients who have fleets of vehicles or equipment (together: "assets"), it is helpful to see certain information about their fleet. Knowing where an asset is or how an asset is being used by the operator is critical to successfully managing a fleet.

Assets often have devices on them that track certain things like the state of the asset or where the asset is at a certain time.

These "trackers" send "incident" data at certain times, like when an asset is turned off or when the asset hits something. This is a large flood of data, and the business person needs to be able to simply see pertinent information extracted from that flood.

### Actual Work

The project uses ECMAScript modules that are native to all modern browsers.  
If you use standard module import statements (relative paths and extensions), then everything will work without any build step.  
It's recommended that you place your work in any of these two places:
* `public/js/views/home.js`
* `public/js/provider/[your name]/*`

The rest of the code is not directly relevant to your work (it is web application boilerplate).

#### Data

You can get all the data for this project by importing the `fetchAll` named export from the `provider/ajax/Incidents/fetchAll.js` module.

Here is an example module that fetches all of the data and logs it to the console:  
```
import { fetchAll } from "../ajax/Incidents/fetchAll.js";

console.log( fetchAll() );
```

`fetchAll` returns an array of Incidents.

### The UI

The UI is provided for you.  

For both of the charts, the UI is expecting the appropriate incidents **grouped by month**. The grouped-by-month incidents array will have the following format:  
```
[
    {
        "date": ISO Date,
        "incidents": [ Incident ... ]
    },
    ...
]
```

The ISO Date for each month can be any valid date that falls within the appropriate month. It may be convenient to use the 1st day of the month so that the data is consistent, but this is not required.

For both of the maps, the UI is expecting a single Location.  
For the starting map, the center should be the Location of the first Incident with the Incident Type `"ignitionOff"`.
For the end map, the center should be the Location of the last Incident with the Incident Type `"ignitionOff"`.

#### Integrating With The UI

As mentioned above in the Actual Work section, your integration code may go in `public/js/views/home.js`. There are 8 commented lines in this file that provide an example implementation. This implementation:  
* is incomplete
* will fail to build if uncommented as is
* is only one of many different ways to complete this project

Notable items in that implementation:  
* The four `.set` calls in the `init` function (in the `"on"` property) are all that is necessary for this view to work - assuming the correct data is provided as their second argument.
* The four `import` statements at the top are not strictly necessary. You could use many fewer or many more.

## FAQ
* How should I write this? What are the syntax rules?
    * You should complete this exercise in a way you feel is professional and complete, as if you were working full time. Other than that, there are no strict guidelines.
* Can I use [y] library? Can I look up the documentation? Can I use a search engine?
    * Yes to all three.
    * Please use anything you think is appropriate to complete the goals. We would never expect you to not use a search engine or documentation pages while you work at EquipmentShare, so we would not expect you to work without those resources in this context either.
    * There are already some libraries available in the project by importing `"[path/to/js/]vendor/[X].js"`. These include (but are not limited to):  
        * moment
    * Feel free to explore anything in the project (including the `package.json` file) to see if there's anything else present you might want to use.
* What if I can't finish in time?
    * That's okay. The saying goes, "Quality over quantity". Work on the goals in order (1 ... 5). There's no hard requirement that you finish everything in the time limit, and if you get done early, we'd love to get a feel for what you think needs to be worked on next.

[iso8601]: https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations
